## FTP搭建
FTP工作方式
```
port方式：主动模式
prot(主动) 方式的连接过程是：客服端向服务器的FTP端口(默认是21)发送连接请求，服务器接受连接，
建立一条命令链路，当需要传送数据时，服务器从20端口向客服端的空闲端口发送连接请求，建立一条数
据链路来传送数据
pasv方式：被动模式
pasv(被动)方式的连接过程是：客服端向服务器的FTP端口（默认是21）发送连接请求，服务器接收连接，
建立一条命令链路，当需要传送数据时 , 客户端向服务器的空闲端口发送连接请求 , 建立一条数据链路
来传送数据

FTP是仅基于tcp的服务 , 不支持udp FTP使用2个端口 , 一个数据端口和一个命令端口（也可叫做控制端口）
通常来说这两个端口是21（命令端口）和20（数据端口） 但FTP工作方式的不同 , 数据端口并不总是20 这就
是主动与被动FTP的最大不同之处

```
### 安装命令
```
yum install -y vsftpd

```

### 配置文件
```
vim /etc/vsftpd/vsftpd.conf
匿名用户上传权限
anonymous_enable=NO
anon_umask=022 #设置本地上传文件权限
anon_upload_enable=YES 允许匿名用户上传文件
anon_other_write_enable=YES #匿名用户对文件删除和重命名
anon_mkdir_write_enable=YES #允许匿名用户创建目录
ftpd_banner=Welcome to blah FTP service. ftp工具连接成功提示
chroot_local_user=YES #不允许切换到上级目录
chroot_list_enable=NO #是否限制在主目录下的名单
userlist_enable=NO
allow_writeable_chroot=YES #不添加这个无法上传文件
anon_root=/usr/local/ftpdir #匿名目录的文件夹
no_anon_password=NO #匿名用户不需要密码
ftp_username=ftpuser    #匿名用户登录后的使用者


```
### 用户
```
useradd ftpuser
password ftpuser
在/usr/local创建一个文件夹
ftpdir

```
### 
```
启动服务
开机自启

```
### 下载ftp客服端
```
yum install -y ftp

```
### 创建777权限文件夹
```
/usr/local/ftpdir里面创建
mkdir files
权限给777
```